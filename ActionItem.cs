﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NoGameNoLife
{
    enum ActionType { CLICK, KEY, MOVE, WAIT, MESSAGE};
    struct ActionItem
    {
        public string type { get; set;}
        public string text{ get; set; }
        public string key{ get; set; }
        public int value { get; set; }
        public int x { get; set; }
        public int y { get; set; }
        public int duration { get; set; }
    }
}
