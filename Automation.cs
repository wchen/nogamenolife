﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NoGameNoLife
{
    struct Automation
    {
        public Key start { get; set; }
        public Key stop { get; set; }
        public int interval { get; set; }
        public List<ActionItem> actions { get; set; }
    }
}
