﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using Newtonsoft.Json;

namespace NoGameNoLife
{
    public partial class Form1 : Form
    {
        private CDD dd;
        private bool hasCompleted = true;
        private Automation automation;
        private Dictionary<int, Timer> hotkeyTimerDict;
        private StreamReader jsonSteamReader;

        public Form1() {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e) {
            string dllFilename = ReadDataFromReg();

            dd = new CDD();
            LoadDllFile(dllFilename);
            label1.Visible = true;
            label2.Visible = true;
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e) {
            unreg_hotkey(automation);
        }

        private void executeAutomations(object sender, EventArgs e) {
            if (!hasCompleted) {
                return;
            } else {
                hasCompleted = false;
                executeAutomation(automation);
                hasCompleted = true;
            }
        }

        private void executeAutomation(Automation automation) {
            if (automation.actions.Count > 0) {
                foreach (ActionItem actionItem in automation.actions) {
                    switch ((ActionType)Enum.Parse(typeof(ActionType), actionItem.type.ToUpper())) {
                        case ActionType.CLICK:
                            clickAction(actionItem.value);
                            break;
                        case ActionType.KEY:
                            keyAction(actionItem.key);
                            break;
                        case ActionType.MOVE:
                            moveAction(actionItem.x, actionItem.y);
                            break;
                        case ActionType.WAIT:
                            System.Threading.Thread.Sleep(actionItem.duration);
                            break;
                        case ActionType.MESSAGE:
                            messageAction(actionItem.text);
                            break;
                    }
                }
            }
        }

        private void keyAction(string key) {
            int ddcode = getDdcode(key);
            dd.key(ddcode, 1);
            System.Threading.Thread.Sleep(5);
            dd.key(ddcode, 2);
            System.Threading.Thread.Sleep(5);
        }

        private void clickAction(int key) {
            dd.btn(key);
            System.Threading.Thread.Sleep(5);
        }

        private void moveAction(int x, int y) {
            dd.mov(x, y);
            System.Threading.Thread.Sleep(5);
        }

        private void messageAction(string text) {
            dd.str(text);
            System.Threading.Thread.Sleep(5);
        }

        private void chooseJsonButtonClickEventHandler(object sender, EventArgs e) {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "*.json|*.json";

            if (ofd.ShowDialog() != DialogResult.OK) {
                return;
            }
            cleanup();
            loadJsonFile(ofd.FileName);
            processJson();
        }

        private void cleanup() {
            if (automation.interval > 0) {
                unloadAutomation(automation);
                jsonSteamReader.Close();
                this.label1.Text = "Start: ";
                this.label2.Text = "Stop: ";
            }
        }

        private void unloadAutomation(Automation automation) {
            unreg_hotkey(automation);

            List<int> timerHotkeys = new List<int>();
            foreach (KeyValuePair<int, Timer> hotkeyTimer in hotkeyTimerDict) {
                timerHotkeys.Add(hotkeyTimer.Key);
            }

            for (int i = timerHotkeys.Count - 1; i >= 0; --i) {
                hotkeyTimerDict.Remove(timerHotkeys[i]);
                timerHotkeys.RemoveAt(i);
            }
        }

        private void loadJsonFile(string filename) {
            string jsonFileContent = "";
            this.jsonInputText.Text = filename;
            jsonSteamReader = new StreamReader(filename);
            jsonFileContent = jsonSteamReader.ReadToEnd();
            automation = JsonConvert.DeserializeObject<Automation>(jsonFileContent);
        }

        private void processJson() {
            addAutomation(automation);
            reg_hotkey(automation);                            // 注册热键
            this.label1.Text = getText("Start: ", automation.start.modifier, automation.start.key);
            this.label2.Text = getText("Stop: ", automation.stop.modifier, automation.stop.key);
        }

        private string getText(string label, string modifier, string key) {
            if (automation.start.modifier != null) {
                return label + modifier + " + " + key;
            } else {
                return label + key;
            }
            
        }

        private void addAutomation(Automation automation) {
            if (hotkeyTimerDict == null) {
                hotkeyTimerDict = new Dictionary<int, Timer>();
            }

            if (hotkeyTimerDict.ContainsKey(getDdcode(automation.start.key))) {
                MessageBox.Show(automation.start.key + " hotkey already exist");
                return;
            }

            if (hotkeyTimerDict.ContainsKey(getDdcode(automation.start.key))) {
                MessageBox.Show(automation.stop.key + " hotkey already exist");
                return;
            }

            Timer timer = new System.Windows.Forms.Timer(this.components);
            timer.Interval = automation.interval;
            timer.Tick += new System.EventHandler(this.executeAutomations);
            hotkeyTimerDict.Add(getDdcode(automation.start.key), timer);
            hotkeyTimerDict.Add(getDdcode(automation.stop.key), timer);
        }

        private void helpLink_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e) {
            string url = "http://" + helpLink.Text;
            System.Diagnostics.Process.Start(url);
        }

        private void LoadDllFile(string dllfile) {
            label1.Visible = false;
            label2.Visible = false;

            System.IO.FileInfo fi = new System.IO.FileInfo(dllfile);
            if (!fi.Exists) {
                MessageBox.Show("文件不存在");
                return;
            }

            int ret = dd.Load(dllfile);

            /*if (ret == -2) { MessageBox.Show("装载库时发生错误"); return; }
            if (ret == -1) { MessageBox.Show("取函数地址时发生错误"); return; }
            if (ret == 0) { MessageBox.Show("非增强模块"); }

            int st = dd.chk();
            switch (st) {
                case -1:
                    MessageBox.Show("DD键盘驱动错误！");
                    break;
                case -2:
                    MessageBox.Show("DD鼠标驱动错误！");
                    break;
                case -3:
                    MessageBox.Show("DD键盘和鼠标驱动错误！");
                    break;
                case 1:
                    MessageBox.Show("DD驱动加载正确！");

                    label1.Visible = true;
                    label2.Visible = true;

                    break;
            }*/
        }
        private string ReadDataFromReg() {
            Microsoft.Win32.RegistryKey key;
            key = Microsoft.Win32.Registry.LocalMachine.OpenSubKey(@"SOFTWARE\\DD XOFT\\", false);
            if (key != null) {
                foreach (string vname in key.GetValueNames()) {
                    if ("path" == vname.ToLower()) {
                        return key.GetValue(vname, "").ToString();
                    }
                }
            }
            return "";
        }

        #region "热键设置相关代码"
        [DllImport("user32.dll")]
        public static extern bool RegisterHotKey(
         IntPtr hWnd,
         int id,                            // 热键标识
         KeyModifiers modkey,  //  修改键
         Keys vk                         //  虚键码
        );
        [DllImport("user32.dll")]
        public static extern bool UnregisterHotKey(
         IntPtr hWnd,              // 窗口句柄 
         int id                          // 热键标识 
        );

        void reg_hotkey(Automation automation) {
            KeyModifiers startModifier = KeyModifiers.None;
            KeyModifiers stopModifier = KeyModifiers.None;
            if (automation.start.modifier != null) {
                startModifier = (KeyModifiers)Enum.Parse(typeof(KeyModifiers), automation.start.modifier);
            }
            if (automation.stop.modifier != null) {
                stopModifier = (KeyModifiers)Enum.Parse(typeof(KeyModifiers), automation.stop.modifier);
            }

            RegisterHotKey(this.Handle, getDdcode(automation.start.key), startModifier, getWindowKeys(automation.start.key));
            RegisterHotKey(this.Handle, getDdcode(automation.stop.key), stopModifier, getWindowKeys(automation.stop.key));
        }

        void unreg_hotkey(Automation automation) {
            if (automation.interval > 0) {
                UnregisterHotKey(this.Handle, getDdcode(automation.start.key));
                UnregisterHotKey(this.Handle, getDdcode(automation.stop.key));
            }
        }

        private Keys getWindowKeys(string key) {
            return (Keys)Enum.Parse(typeof(Keys), key);
        }

        private int getDdcode(string key) {
            int digit;
            bool isNumeric = int.TryParse(key, out digit);
            
            if (isNumeric) {
                key = 'D' + key;
            }

            int winKey = (int)getWindowKeys(key);
            return dd.todc(winKey);
        }

        protected override void WndProc(ref Message m) {
            const int WM_HOTKEY = 0x0312;                        //0x0312表示用户热键
            switch (m.Msg) {
                case WM_HOTKEY:
                    ProcessHotkey(m);                                      //调用ProcessHotkey()函数
                    break;
            }
            base.WndProc(ref m);
        }

        private void ProcessHotkey(Message msg)              //按下设定的键时调用该函数
        {
            if (msg.WParam.ToInt32() == getDdcode(automation.start.key)) {
                StartHotkey(getDdcode(automation.start.key));
            } else if (msg.WParam.ToInt32() == getDdcode(automation.stop.key)) {
                StopHotkey(getDdcode(automation.stop.key));
            }
        }

        private void StartHotkey(int ddcode) {
            hotkeyTimerDict[ddcode].Enabled = true;
            statusButton.Text = "Enabled";
        }

        private void StopHotkey(int ddcode) {
            hotkeyTimerDict[ddcode].Enabled = false;
            statusButton.Text = "Disabled";
        }

        #endregion

    }
}

