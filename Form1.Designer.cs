﻿namespace NoGameNoLife
{
    partial class Form1
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            this.statusButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.helpLink = new System.Windows.Forms.LinkLabel();
            this.jsonInputText = new System.Windows.Forms.TextBox();
            this.chooseJsonButton = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // statusButton
            // 
            this.statusButton.Enabled = false;
            this.statusButton.Location = new System.Drawing.Point(144, 152);
            this.statusButton.Name = "statusButton";
            this.statusButton.Size = new System.Drawing.Size(75, 25);
            this.statusButton.TabIndex = 1;
            this.statusButton.Text = "Diabled";
            this.statusButton.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Start: ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(15, 46);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "Stop: ";
            // 
            // helpLink
            // 
            this.helpLink.AutoSize = true;
            this.helpLink.Location = new System.Drawing.Point(133, 197);
            this.helpLink.Name = "helpLink";
            this.helpLink.Size = new System.Drawing.Size(86, 13);
            this.helpLink.TabIndex = 5;
            this.helpLink.TabStop = true;
            this.helpLink.Text = "www.ddxoft.com";
            this.helpLink.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.helpLink_LinkClicked);
            // 
            // jsonInputText
            // 
            this.jsonInputText.Location = new System.Drawing.Point(12, 12);
            this.jsonInputText.Name = "jsonInputText";
            this.jsonInputText.ReadOnly = true;
            this.jsonInputText.Size = new System.Drawing.Size(337, 20);
            this.jsonInputText.TabIndex = 6;
            // 
            // chooseJsonButton
            // 
            this.chooseJsonButton.Location = new System.Drawing.Point(127, 38);
            this.chooseJsonButton.Name = "chooseJsonButton";
            this.chooseJsonButton.Size = new System.Drawing.Size(118, 25);
            this.chooseJsonButton.TabIndex = 0;
            this.chooseJsonButton.Text = "Choose Json";
            this.chooseJsonButton.UseVisualStyleBackColor = true;
            this.chooseJsonButton.Click += new System.EventHandler(this.chooseJsonButtonClickEventHandler);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(109, 69);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(149, 77);
            this.groupBox1.TabIndex = 10;
            this.groupBox1.TabStop = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(358, 234);
            this.Controls.Add(this.jsonInputText);
            this.Controls.Add(this.chooseJsonButton);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.statusButton);
            this.Controls.Add(this.helpLink);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.Text = "No Game No Life " + getVersion();
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        private string getVersion() {
            string version = "";
            try {
                System.Version ver = System.Deployment.Application.ApplicationDeployment.CurrentDeployment.CurrentVersion;
                version = string.Format("{0}.{1}.{2}.{3}", ver.Major, ver.Minor, ver.Build, ver.Revision);
            } catch (System.Deployment.Application.InvalidDeploymentException e) {
                version = "<development>";
            }

            return version;
        }

        #endregion

        private System.Windows.Forms.Button statusButton;
        private System.Windows.Forms.LinkLabel helpLink;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox jsonInputText;
        private System.Windows.Forms.Button chooseJsonButton;
        private System.Windows.Forms.GroupBox groupBox1;
    }
}

