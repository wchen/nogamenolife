﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NoGameNoLife
{
    struct Key
    {
        public string modifier { get; set; }
        public string key { get; set; }
    }
}
